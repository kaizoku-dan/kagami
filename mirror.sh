#!/bin/bash

set -eux

NAME=$1
SOURCE_URL=$2
TARGET_URL=$3
WITH_LFS=$4

if [[ -d "$NAME.git" ]]; then
    cd "$NAME.git"
    git fetch --prune --tags --prune-tags "$SOURCE_URL" "+refs/*:refs/*"

    if [[ "$WITH_LFS" == "true" ]]; then
        git lfs fetch --all
        git lfs push --all "$TARGET_URL"
    fi
else
    git clone --bare "$SOURCE_URL" "$NAME.git"
    cd "$NAME.git"

    if [[ "$WITH_LFS" == "true" ]]; then
        git lfs fetch --all
        git lfs push --all "$TARGET_URL"
    fi
fi

git push --mirror "$TARGET_URL"
